#include "calculator.h"

void Calculator::calculate_record(const CellPhoneRecords::Record& rec) {
  if(month == 42) {
    init(rec);
  }

  if(rec.date.tm_mon != month && rec.date.tm_year != year) {
    push_fee();
    init(rec);
  }

  switch(rec.type) {
    case CellPhoneRecords::RecordType::VS: {
      calculate_call(rec, mcfg.feepermininternal, mcurentfee.feeinternalminutes, mcurentfee.internalminutescount);
      break;
    }
    case CellPhoneRecords::RecordType::VM: {
      calculate_call(rec, mcfg.feepreminexternal, mcurentfee.feeexternalminutes, mcurentfee.externalminutescount);
      break;
    }
    case CellPhoneRecords::RecordType::SS: {
      calculate_sms(rec, mcfg.smsinternal, mcurentfee.feeinternalsms, mcurentfee.internalsmscount);
      break;
    }
    case CellPhoneRecords::RecordType::SM: {
      calculate_sms(rec, mcfg.smsexternal, mcurentfee.feeexternalsms, mcurentfee.externalsmscount);
      break;
    }
  };
}

void Calculator::push_fee() {
  mcurentfee.year = year + 1900;
  mcurentfee.month = month + 1;

  mcurentfee.fee += mcurentfee.feeinternalminutes + mcurentfee.feeexternalminutes +
                    mcurentfee.internalsmscount + mcurentfee.feeexternalsms;

  mfees.push_back(mcurentfee);
}

void Calculator::print_fees() const {

  for(const auto& fee: mfees) {
    std::cout << "Fee for: " << fee.year << "/" << fee.month
              << "\n\tCharged sum:" << fee.fee
              << "\n\tMinutes in network: " << fee.internalminutescount << ", charged sum:" << fee.feeinternalminutes
              << "\n\tMinutes outside network: " << fee.externalminutescount << ", charged sum:" << fee.feeexternalminutes
              << "\n\tSms in network: " << fee.internalsmscount << ", charged sum:" << fee.feeinternalsms
              << "\n\tSms outside network: " << fee.externalsmscount << ", charged sum:" << fee.feeexternalsms
              << std::endl;
  }
}

void Calculator::init(const CellPhoneRecords::Record& rec) {
  mcurentfee = Fee{};
  mcfg = Config{};

  year = rec.date.tm_year;
  month = rec.date.tm_mon;

  mcurentfee.fee = mcfg.monthlyfee;
}

void Calculator::calculate_call(const CellPhoneRecords::Record& rec, float feepermin, float& fee, float& mincount) {
  mincount += static_cast<float>(rec.duration) / 60.0f;
  if(mcfg.freenumbers.count(rec.number) > 0) {
    return;
  }
  float duration = rec.duration;
  if(rec.duration <= mcfg.freeminutes) {
    mcfg.freeminutes -= rec.duration;
    return;
  }
  else if(mcfg.freeminutes > 0) {
    duration = rec.duration - mcfg.freeminutes;
  }

  if(duration <= 60) {
    fee += feepermin;
  }
  else {
    fee += (duration / 60.0f) * feepermin;
  }
}

void Calculator::calculate_sms(const CellPhoneRecords::Record& rec, float feepersms, float& fee, size_t& smscount) {
  ++smscount;
  if(mcfg.freenumbers.count(rec.number) > 0) {
    return;
  }
  if(mcfg.freesms > 0) {
    mcfg.freesms -= 1;
    return;
  }

  fee += feepersms;
}
