#pragma once

#include <ctime>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>
#include <string>

class CellPhoneRecords {
  public:
    enum class RecordType {
      VS,
      VM,
      SS,
      SM,
    };
    struct Record {
      std::tm date;
      std::string number;
      size_t duration;
      RecordType type;
    };
    static CellPhoneRecords from_file(const std::filesystem::path& dbpath);

    void for_each_record(std::function<void(const Record&)> fn) {
      for(const auto& rec: mrecords) {
        fn(rec);
      }
    }
  protected:
    CellPhoneRecords() = default;

    void read_file(std::istream&);
    std::ifstream open_file(const std::filesystem::path&);
  private:
    std::vector<Record> mrecords;
};
