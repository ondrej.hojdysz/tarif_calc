#include <iostream>

#include "calculator.h"
#include "cellphonerecords.h"

int main(int argc, char** argv) {
  if(argc < 2) {
    std::cerr << "Expected csv file" << std::endl;
    return -42;
  }

  auto records = CellPhoneRecords::from_file(argv[1]);
  Calculator calc;
  records.for_each_record([&calc](const CellPhoneRecords::Record& rec) mutable {calc.calculate_record(rec);});
  calc.push_fee();
  calc.print_fees();
  return 0;
}
