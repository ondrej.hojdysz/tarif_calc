#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

#include "cellphonerecords.h"

class Calculator {
  public:
    struct Config {
      size_t freeminutes = 100 * 60,
             freesms = 10;
      float feepermininternal = 1.5,
            feepreminexternal = 3.5,
            smsinternal = 1,
            smsexternal = 2,
            monthlyfee = 900;
      std::unordered_set<std::string> freenumbers = {"+420732563345", "+420707325673"};

    };

    struct Fee {
      float fee = 0,
            feeinternalsms = 0,
            feeinternalminutes = 0,
            feeexternalsms = 0,
            feeexternalminutes = 0,
            internalminutescount = 0,
            externalminutescount = 0;
      size_t internalsmscount = 0,
             externalsmscount = 0;
      int year = 0,
          month = 0;
    };

    void calculate_record(const CellPhoneRecords::Record& rec);
    void push_fee();
    void print_fees() const;
  private:
    void init(const CellPhoneRecords::Record& rec);
    void calculate_call(const CellPhoneRecords::Record& rec, float feepermin, float& fee, float& mincount);
    void calculate_sms(const CellPhoneRecords::Record& rec, float feepersms, float& fee, size_t& smscount);

    int month = 42,
        year = 0;
    std::vector<Fee> mfees;
    Fee mcurentfee;
    Config mcfg;

};
