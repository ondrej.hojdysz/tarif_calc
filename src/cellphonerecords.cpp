#include <algorithm>
#include <array>
#include <cctype>
#include <iomanip>
#include <sstream>

#include "cellphonerecords.h"

using StrArray = std::array<std::string, 4>;

bool split_by_semicolon(StrArray& split, const std::string& str);
bool parse(CellPhoneRecords::Record& record, const StrArray& data);
bool parse_time(std::tm&, const std::string&);
bool parse_number(std::string&, const std::string&);
bool parse_duration(size_t& duration, const std::string&);
bool parse_record_type(CellPhoneRecords::RecordType& type,  const std::string&);

CellPhoneRecords CellPhoneRecords::from_file(const std::filesystem::path& path) {
  CellPhoneRecords records;
  if(!std::filesystem::is_regular_file(path)) {
    std::cerr << "Database file not found, creating empty database!!!" << std::endl;
    return records;
  }

  auto file = records.open_file(path);
  records.read_file(file);
  return records;
}

std::ifstream CellPhoneRecords::open_file(const std::filesystem::path& path) {
  std::ifstream fs;

  fs.open(path, std::ios_base::in);

  return fs;
}

void CellPhoneRecords::read_file(std::istream& instream) {
  std::string line;

  std::getline(instream, line);

  StrArray splited;
  while(std::getline(instream, line)) {
    if(!split_by_semicolon(splited, line)) {
      std::cerr << "File contains bad record: "<< line << "!" << std::endl;
      continue;
    }

    Record record;
    if(!parse(record, splited)) {
      std::cerr << "File contains bad record: "<< line << "!" << std::endl;
      continue;
    }

    mrecords.push_back(record);
  }
}

bool split_by_semicolon(StrArray& split, const std::string& str) {
  std::string::size_type index = str.find(';'),
                         last_index = 0;

  StrArray::size_type arr_index = 0;

  do {
    index = str.find(';', last_index);
    split[arr_index] = str.substr(last_index, index - last_index);

    last_index = index;
    ++last_index;
    ++arr_index;

  } while(index < str.size());

  split[3].erase(
      std::remove_if(split[3].begin(), split[3].end(), [] (auto c) -> bool {return std::isspace(c);}),
      split[3].end()
  );
  return arr_index == 4;
}

bool parse(CellPhoneRecords::Record& record, const StrArray& data) {
  if(!parse_time(record.date, data[0])) {
    std::cerr << "TimeParse failed: " << data[0] << std::endl;
    return false;
  }
  if(!parse_number(record.number, data[1])) {
    std::cerr << "NumberParse failed: " << data[1] << std::endl;
    return false;
  }
  if(!parse_duration(record.duration, data[2])) {
    std::cerr << "DurationParse failed: " << data[2] << std::endl;
    return false;
  }
  if(!parse_record_type(record.type, data[3])) {
    std::cerr << "RecordTypeParse failed: " << data[3] << std::endl;
    return false;
  }
  return true;
}

void add_record(const CellPhoneRecords::Record& record) {
}
bool parse_time(std::tm& time, const std::string& str) {
  std::stringstream s;

  s << str;

  s >> std::get_time(&time, "%d.%m.%Y %H:%M:%S");

  return !s.fail();
}

bool parse_number(std::string& number, const std::string& str) {
  number = str;
  return true;
}

bool parse_duration(size_t& duration, const std::string& str) {
  if(str.empty()) {
    duration = 0;
    return true;
  }
  try {
    duration = std::stoull(str);
  } catch(...) {
    return false;
  }
  return true;
}

bool parse_record_type(CellPhoneRecords::RecordType& type,  const std::string& str) {
  if(str == "VS") {
    type = CellPhoneRecords::RecordType::VS;
    return true;
  }
  if(str == "SS") {
    type = CellPhoneRecords::RecordType::SS;
    return true;
  }
  if(str == "VM") {
    type = CellPhoneRecords::RecordType::VM;
    return true;
  }
  if(str == "SM") {
    type = CellPhoneRecords::RecordType::SM;
    return true;
  }
  return false;
}
